# Survivalrun opdracht

* Demo: http://jeroen-van-veen.nl/survivalrun/
* Source: https://bitbucket.org/jvanveen/survivalrun

## Benodigdheden
* Node.js 8+
* npm 5


## Installatie
```
git clone git@bitbucket.org:jvanveen/survivalrun.git
cd survivalrun
npm i
gulp build watch
```

Ga naar `localhost:8999` om de applicatie te bekijken.


## Opdracht benadering
In de opdrachtbeschrijving wordt de nadruk gelegd op 'User Experience'.
Ik heb de indruk dat het de voorkeur geniet om een niet-werkend concept
uit te werken qua UX. Zowel het uitwerken van de beschreven functionaliteit als
de UX is naar mijn mening een onmogelijke opdracht voor de 8 uur die ervoor
staan.

Mijn inval en toegevoegde waarde zit niet zozeer in het uitdenken van een
goede User Interface, maar meer in het bouwen en uitdenken van de
functionaliteit. User experience staat of valt naar mijn
mening bij een goede implementatie.

50mb data in de browser laden bij het openen van een webpagina is bij
voorbaat al een User Experience issue van formaat. Ik heb daarom de tijd besteed
aan het uitwerken van dit probleem: hoe kan die hoeveelheid data
op een handige manier ingeladen en gecached worden? Dit is een basisvoorwaarde
om vervolgens de data gemakkelijk en efficient te kunnen bevragen.

Ik ben allereerst begonnen met een Vue/Gulp project template dat ik eerder heb gemaakt.
Vue is een lichtgewicht viewmodel waarmee snel User Interfaces inelkaar gesleuteld
kunnen worden aan de hand van een data model. Met Gulp zorg ik voor JavaScript file
bundling, SCSS preprocessing, livereload tijdens ontwikkeling, etc..  

Daarna heb ik gekeken hoe ik data het beste kan cachen en queryen. Dat kan
eigenlijk alleen goed met Indexeddb. Om de Indexeddb API toegankelijker te maken
heb ik een wrapper library gebruikt(Dexie.js).

Bij de eerste keer inladen worden 4 scripts geinjecteerd in de browser
met de json-data en vervolgens weggeschreven naar Indexeddb. Een wat primitieve
progressbar laat de voortgang hiervan zien aan de gebruiker, zodat die niet het
idee heeft dat de applicatie blijft hangen.

Aan het einde wordt de staat van de applicatie opgeslagen in localStorage. Deze
geeft o.a. aan dat de volledige dataset is ingeladen. Hierdoor wordt de applicatie
de 2e keer bijna instant geladen. Ik heb vervolgens de steden in een aparte tabel
gezet en heb een simpele dropdown toegevoegd waarin gebruikers wedstrijden kunnen
laten zien die in een bepaalde stad plaatsvinden.

Voor de kaart heb ik Leaflet gebruikt. Er was al een Leaflet component voor Vue
beschikbaar. Die component maakt het gemakkelijker om Leaflet reactief op data
te laten reageren. Het ging me met name erom dat ik de markers via Vue data
kon aansturen. Vervolgens heb ik nog wat aan filters zitten sleutelen
en heb 2 voorbeeldjes gemaakt van het aanroepen van de data. Ik kwam er
daardoor o.a. achter dat Indexeddb/Dexie.js helaas geen chaining kan doen
van AND queries. Daarmee was de tijd ook al op.


### Wat ging goed
Ik denk dat ik in 8 uur een goede opzet heb gemaakt van een applicatie die
reactief op grotere hoeveelheden data kan reageren en de resultaten effectief
kan tonen in een kaart.


### Wat kon beter
Ik heb geen of weinig aandacht besteed aan het type filters. Ook heb ik weinig
tijd besteed aan het uitdenken van hoe de gebruiker deze data het beste kan
inzien, behalve door een paar simpele voorbeeldjes te maken van Indexeddb
queries. Er is redelijk veel data beschikbaar, maar er was simpelweg niet
genoeg tijd om bijv. geo-queries uit te zoeken, verbanden tussen atleten
te onderzoeken en om alternatieve user interfaces uit te zoeken.
Voor een deel van de data is een kaart namelijk waarschijnlijk helemaal
geen goede oplossing (bijv. om verbanden tussen vrienden/atleten en
wedstrijden te tonen), maar dat heb ik voor deze opdracht verder genegeerd.


### Mijn indruk
Ik vond het interessant om de opdracht uit te werken en iets te bedenken voor het
data-probleem, maar ik vond de scope te ruim en de tijd te kort. Hierdoor heb ik
een paar vraagtekens:
* Ligt het zwaartepunt van de vacature nu bij een UX'er of een frontend JavaScript developer?
* Was het een onderdeel van de opdracht om de opdrachtgever om meer informatie te vragen?
* In hoeverre is scoping bij echte projecten een probleem?

Ik hoop dat we deze punten 3 april even kunnen bespreken.
