const App = require('../../shared/js/lib/app')


let env = JSON.parse(JSON.stringify(require('../../shared/js/lib/env')))

/**
* Main DOM user interface implementation.
*/
class FrontendApp extends App {

    constructor(options) {
        options.env = env
        super(options)

        if (this.env.isChrome) $('html').classList.add('chrome')
        if (this.env.isEdge) $('html').classList.add('edge')
        if (this.env.isFirefox) $('html').classList.add('firefox')

        this.env = env
        this.on('fg:notify', (message) => this.vm.$notify(message))
        /**
        * Syncs state from the backend to the frontend
        * while keeping Vue's reactivity system happy.
        */
        this.on('fg:set_state', this.__mergeState.bind(this))

        // Register application components here.
        Vue.component('Field', require('../../shared/components/field')(this))
        Vue.component('MainHeader', require('../../shared/components/main_header')(this))

        Vue.component('v-map', Vue2Leaflet.Map)
        Vue.component('v-tilelayer', Vue2Leaflet.TileLayer)
        Vue.component('v-marker', Vue2Leaflet.Marker)

        Vue.component('Filters', require('../../shared/components/filters')(this))
        Vue.component('Map', require('../../shared/components/map')(this))
        Vue.component('Notifications', require('../../shared/components/notifications')(this))

        // Survivalrun components
        Vue.component('Importer', require('../../shared/components/importer')(this))

        this.initStore()
    }


    initStore() {
        super.initStore()
        // Initialize with the initial state from the background.
        this.emit('bg:get_state', {
            callback: (state) => {
                this.state = JSON.parse(state)
                this.initViewModel()
                this.vm.$mount(document.querySelector('#app-placeholder'))
                this.setState({ui: {visible: true}})

                if (this.env.isExtension) {
                    // Keep track of the popup's visibility status by
                    // opening a long-lived connection to the background.
                    browser.runtime.connect({name: 'frontend'})
                }
            },
        })
    }
}

if (!global.app) global.app = {}
if (!global.app.bg.ready) {
    global.app.bg.on('ready', () => {
        global.app.fg = new FrontendApp({apps: {bg: global.app.bg}, name: 'fg'})
    })
} else {
    global.app.fg = new FrontendApp({apps: {bg: global.app.bg}, name: 'fg'})
}



module.exports = FrontendApp
