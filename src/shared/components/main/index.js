module.exports = (app) => {

    return {
        computed: app.helpers.sharedComputed(),
        render: templates.main.r,
        staticRenderFns: templates.main.s,
        store: {
            importer: 'importer',
            map: 'map',
            user: 'user',
        },
    }
}
