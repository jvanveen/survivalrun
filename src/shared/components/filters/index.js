module.exports = (app) => {

    return {
        computed: app.helpers.sharedComputed(),
        data: function() {
            return {
                obstacles: 20,
                filter: {
                    city: {enabled: false, value: {id: null, name: null}},
                    heavyObstacles: {enabled: true, value: 1},
                    obstacles: {enabled: true, value: 30},
                },
            }
        },
        methods: {
            applyFilters: async function() {
                let _filters = {}

                const res = await app.store.db.wedstrijden
                    .where('aantal_hindernissen')
                    .below(parseInt(this.filter.obstacles.value)).and((value) => {
                        let match = false

                        if (this.filter.heavyObstacles.enabled) {
                            match = (value.aantal_zware_hindernissen > this.filter.heavyObstacles.value)
                        }

                        // Apply city filter.
                        if (match && this.filter.city.enabled && this.filter.city.value.name) {
                            match = (value.city === this.filter.city.value.name)
                        }
                        return match
                    }).limit(255).toArray()
                this.markers = res.map((i) => [i.latitude, i.longitude])
            }
        },
        mounted: function() {
            this.applyFilters()
        },
        render: templates.filters.r,
        staticRenderFns: templates.filters.s,
        store: {
            filters: 'map.filters',
            markers: 'map.markers',
            zoom: 'map.zoom',
        },
        watch: {
            filter: {
                deep: true,
                handler: function(val) {
                    this.applyFilters()
                }
            },
            'filter.obstacles.value': function(newVal, oldVal) {
                if (this.filter.heavyObstacles.value >= newVal) {
                    this.filter.heavyObstacles.value = newVal - 1
                }
            }
        },
    }
}
