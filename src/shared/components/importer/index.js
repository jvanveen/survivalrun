module.exports = (app) => {

    return {
        computed: app.helpers.sharedComputed(),
        methods: {
            importData: function() {

            },
        },
        mounted: function() {
            if (!this.imported) app.emit('bg:importer:import')
        },
        render: templates.importer.r,
        staticRenderFns: templates.importer.s,
        store: {
            imported: 'importer.imported',
            progress: 'importer.progress',
            status: 'importer.status',
        },
    }
}
