module.exports = (app) => {

    return {
        computed: app.helpers.sharedComputed(),
        render: templates.main_header.r,
        staticRenderFns: templates.main_header.s,
        store: {},
    }
}
