module.exports = (app) => {

    return {
        computed: app.helpers.sharedComputed(),
        data: function() {
            return {
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, &copy; <a href="https://carto.com/attribution">CARTO</a>',
                center: [52.1, 5.9],
                url: 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png',
                zoom: 7,
            }
        },
        render: templates.map.r,
        staticRenderFns: templates.map.s,
        store: {
            filters: 'map.filters',
            markers: 'map.markers',
        },
    }
}
