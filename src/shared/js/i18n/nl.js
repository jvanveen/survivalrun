/*eslint max-len:0 */
/*eslint quote-props: 0 */
global.translations.nl = {
    'congratulations': 'gefeliciteerd',
    'Find a contest': 'Zoek een wedstrijd',
    'Max. obstacles': 'Max. aantal obstakels',
    'Min. heavy obstacles': 'Min. aantal zware obstakels',
    'Limit to city': 'Beperk tot stad',
    'Select a city': 'Selecteer een stad'
}
