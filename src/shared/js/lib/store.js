/**
* A simple localstorage store.
*/
class Store {

    constructor(app) {
        this.app = app
        this.dbSchema = '1.0'

        this.db = new Dexie('survivalrun', {addons: [relationships]})
        // Declare tables, IDs and indexes
        this.db.version(1).stores({
            atleten: '++id,naam,city,longitude,latitude,niveau_afstand,niveau_hindernissen,typical_auto,population',
            cities: '++id,name',
            deelnames: '++id,wedstrijd -> wedstrijden.id,atleet -> atleten.id',
            vrienden: '++id,atleet1Id -> athlete.id,atleet2Id -> athlete.id',
            wedstrijden: '++id,city,longitude,afstand,sluiting_inschrijven,population,vrije_plekken,afstanden,aantal_hindernissen,opening_inschrijven,' +
                         'latitude,inschrijvingen,datum,aantal_zware_hindernissen',
        })

    }


    clear() {
        localStorage.clear()
    }


    get(key) {
        if (this.app.verbose) this.app.logger.debug(`${this}get value for key '${key}'`)
        var value = localStorage.getItem(key)
        if (value) {
            return JSON.parse(value)
        }
        return null
    }


    /**
    * Import data collections from JSON dumps to Indexeddb.
    * @param {String} collection - Collection name to import.
    * @returns {Promise} - Resolves after database import.
    */
    import(collection) {
        const batchSize = 100
        return new Promise((resolve, reject) => {
            this.app.helpers.injectScript(`js/data-${collection}.js`, async() => {
                const iterations = Math.ceil(global.data[collection].length / batchSize)
                for (let i = 0; i < iterations; i++) {
                    const dataSubset = global.data[collection].slice((i * batchSize), ((i + 1) * batchSize))

                    await this.db.transaction('rw', this.db[collection], () => {
                        this.db[collection].bulkPut(dataSubset)
                    })

                    resolve()
                }
            })
        })
    }


    async generateFilterData() {
        let _cities = {}
        for (const wedstrijd of await this.db.wedstrijden.toArray()) {
            _cities[wedstrijd.city] = true
        }

        for (const city of Object.keys(_cities)) {
            this.db.cities.add({name: city})
        }
    }


    remove(key) {
        if (this.get(key)) {
            localStorage.removeItem(key)
        }
    }


    reset() {
        localStorage.clear()
    }


    set(key, value) {
        localStorage.setItem(key, JSON.stringify(value))
    }


    toString() {
        return `${this.app}[store] `
    }


    validSchema() {
        let schema = this.get('db_schema')
        if (!schema || schema !== this.dbSchema) {
            this.set('db_schema', this.dbSchema)
            this.app.logger.warn(`${this}storage schema change detected! db: ${schema} state: ${this.dbSchema}`)
            return false
        }

        return true
    }
}

module.exports = Store
