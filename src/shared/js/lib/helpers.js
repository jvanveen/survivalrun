// Actions shared across components. Don't modify state local component
// state from here.
module.exports = function(app) {

    let helpers = {}

    /**
    * Translations that are shared across components
    * can be defined here using key/value.
    * @returns {Object} - Translated key/value.
    */
    helpers.getTranslations = function() {
        const $t = app.$t
        return {
            congratulations: $t('congratulations'),
        }
    }

    helpers.sharedMethods = function() {
        return {

        }
    }

    helpers.sharedComputed = function() {
        return {

        }
    }


    helpers.validators = {
        // Regex source: https://github.com/johnotander/domain-regex/blob/master/index.js
        domain: function(e) {
            let res = e.match(/\b((?=[a-z0-9-]{1,63}\.)(xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}\b/)
            if (!res) return false
            return true
        },
    }


    /**
     * Inject a script in the HEAD of the page. Useful to load
     * new translations.
     * @param {String} src - The source of the script.
     * @param {Function} cb - Function to call when the script is loaded.
     */
    helpers.injectScript = function(src, cb) {
        let script = document.createElement('script')
        script.type = 'text/javascript'

        script.onload = function() {
            // Cleanup onload handler.
            script.onload = null
            cb()
        }

        // Add the script to the DOM.
        document.getElementsByTagName('head')[0].appendChild(script)
        // Set the `src` to begin transport.
        script.src = src
    }


    return helpers
}
