const Skeleton = require('./skeleton')

/**
* Main entrypoint for the Application.
*/
class App extends Skeleton {

    constructor(options) {
        super(options)

        this.env = options.env

        // Component helpers.
        this.helpers = require('./helpers')(this)
        this.utils = require('./utils')

        this._modules = options.modules
        this.modules = {}

        if (this.name === 'bg') this._emitTarget = 'fg'
        else if (this.name === 'fg') this._emitTarget = 'bg'
    }


    __isObject(item) {
        return (item && typeof item === 'object' && !Array.isArray(item))
    }


    __mergeDeep(target, ...sources) {
        if (!sources.length) return target
        const source = sources.shift()

        if (this.__isObject(target) && this.__isObject(source)) {
            for (const key in source) {
                if (this.__isObject(source[key])) {
                    if (!target[key]) Object.assign(target, { [key]: {} })
                    this.__mergeDeep(target[key], source[key])
                } else {
                    Object.assign(target, { [key]: source[key] })
                }
            }
        }

        return this.__mergeDeep(target, ...sources)
    }


    /**
    * Vue-friendly object operations.
    */
    __mergeState({action, path, persist, state}) {
        if (path) {
            path = path.split('/')
            if (action === 'insert') {
                this.__setFromPath(this.state, path, state)
            } else if (action === 'merge') {
                const _ref = path.reduce((o, i)=>o[i], this.state)
                this.__mergeDeep(_ref, state)
            } else if (action === 'delete') {
                const _ref = path.slice(0, path.length - 1).reduce((o, i)=>o[i], this.state)
                Vue.delete(_ref, path[path.length - 1])
            } else if (action === 'replace') {
                const _ref = path.slice(0, path.length - 1).reduce((o, i)=>o[i], this.state)
                Vue.set(_ref, path[path.length - 1], state)
            }

        } else {
            this.__mergeDeep(this.state, state)
        }
        if (persist) this.store.set('state.unencrypted', this.state)
    }


    __setFromPath(obj, is, value) {
        if (is.length === 1) {
            if (!obj[is[0]]) Vue.set(obj, is[0], value)
            return obj[is[0]]
        } else if (is.length === 0) {
            return obj
        } else {
            return this.__setFromPath(obj[is[0]], is.slice(1), value)
        }
    }


    /**
    * Create a I18n stash store and pass it to the I18n plugin.
    */
    initI18n() {
        const i18nStore = new I18nStore(this.store)
        Vue.use(i18n, i18nStore)

        let selectedLanguage = this.state.settings.language
        for (const translation of Object.keys(translations)) {
            Vue.i18n.add(selectedLanguage, translations[translation])
        }
        Vue.i18n.set(selectedLanguage)
        // Add a simple reference to the translation module.
        this.$t = Vue.i18n.translate
    }


    /**
    * Application parts using this class should provide their own
    * initStore implementation. The foreground script for instance
    * gets its state from the background, while the background
    * gets its state from localstorage or from a
    * hardcoded default fallback.
    */
    initStore() {
        this.state = {
            env: this.env,
            settings: {
                language: 'en',
            },
        }
    }


    initViewModel(watchers, state) {
        this.initI18n()
        this.vm = new Vue({
            data: {
                store: this.state,
            },
            render: h => h(require('../../components/main')(this)),
            watch: watchers,
        })
    }


    /**
    * Load all modules.
    */
    loadModules() {
        // Init these modules.
        for (let module of this._modules) {
            this.modules[module.name] = new module.Module(this)
        }
    }


    /**
    * Set the background state and propagate it to the other end.
    * @param {Object} state - The state to update.
    * @param {Boolean} options - Whether to persist the changed state to localStorage.
    */
    setState(state, {action, path, persist} = {}) {
        if (!action) action = 'merge'
        this.__mergeState({action, path, persist, state})

        this.emit(`${this._emitTarget}:set_state`, {
            action,
            path,
            persist,
            state: this.env.isExtension ? state : JSON.parse(JSON.stringify(state)),
        })
    }
}

module.exports = App
