window.global = window
global.Dexie = require('dexie')
global.relationships = require('dexie-relationships')


global.$ = document.querySelector.bind(document)
global.$$ = document.querySelectorAll.bind(document)

global.resizeSensor = require('css-element-queries').ResizeSensor

global.EventEmitter = require('eventemitter3')
if (!global.translations) global.translations = {}

global.Vue = require('vue/dist/vue.runtime')


// Define custom tags here, so they will not get interpreted by Vue.
Vue.config.ignoredElements = ['component']

global.Vue2Leaflet = require('vue2-leaflet')
global.VueStash = require('vue-stash').default
Vue.use(global.VueStash)

global.i18n = require('vue-stash-i18n')
global.I18nStore = require('vue-stash-i18n/src/store-stash')

if (process.env.NODE_ENV === 'production') {
    Vue.config.productionTip = false
    Vue.config.devtools = false
}

global.Vuelidate = require('vuelidate')
global.Vuelidate.validators = require('vuelidate/lib/validators')
Vue.use(global.Vuelidate.default)

global.VueSVGIcon = require('vue-svgicon')
Vue.use(global.VueSVGIcon, {
    tagName: 'svgicon',
})
