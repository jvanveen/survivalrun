const App = require('../../shared/js/lib/app')


let env = JSON.parse(JSON.stringify(require('../../shared/js/lib/env')))

/**
* Main DOM user interface implementation.
*/
class BackendApp extends App {

    constructor(options) {
        options.env = env
        super(options)
        this.ready = false

        this.env = env
        this.on('fg:notify', (message) => this.vm.$notify(message))
        /**
        * Syncs state from the backend to the frontend
        * while keeping Vue's reactivity system happy.
        */
        this.on('fg:set_state', this.__mergeState.bind(this))
        // Send this script's state back to the requesting script.
        this.on('bg:get_state', (data) => {
            data.callback(JSON.stringify(this.state))
        })

        this.modules = {}
        // Initialize all modules.
        for (let module of options.modules) {
            this.modules[module.name] = new module.Module(this)
        }

        this._init()

    }

    async _init() {
        await this.initStore()
    }


    /**
    * Provide the initial application state when no state is
    * available in localstorage.
    * @returns {Object} - The initial Vue-stash structure.
    */
    _initialState() {
        let state = {}
        for (let moduleName of Object.keys(this.modules)) {
            if (this.modules[moduleName]._initialState) {
                state[moduleName] = this.modules[moduleName]._initialState()
            }
        }

        return state
    }


    /**
    * Sets the state back to defaults without losing some
    * all personalized general settings and preferences.
    * @returns {Object} Stripped state.
    */
    _resetState() {
        let _state = this._initialState()
        delete _state.app

        Object.assign(_state, {
            settings: {
                language: 'en',
            },
            ui: {layer: 'login'},
        })

        return _state
    }


    async initStore() {
        super.initStore()
        Object.assign(this.state, this._initialState())

        const unencryptedState = this.store.get('state.unencrypted')
        if (typeof unencryptedState === 'object') {
            this.__mergeDeep(this.state, unencryptedState)
        }

        let watchers = {}

        // Each module can define watchers on store attributes, which makes
        // it easier to centralize data-related logic.
        for (let module of Object.keys(this.modules)) {
            if (this.modules[module]._watchers) {
                Object.assign(watchers, this.modules[module]._watchers())
            }
        }

        this.initViewModel(watchers, this.state)

        // (!) State is reactive from here on.
        for (let module of Object.keys(this.modules)) {
            if (this.modules[module]._ready) this.modules[module]._ready()
        }

        this.ready = true
        this.emit('ready')
    }
}

if (!global.app) global.app = {}
global.app.bg = new BackendApp({
    modules: [
        {Module: require('./modules/importer'), name: 'importer'},
        {Module: require('./modules/map'), name: 'map'},
    ],
    name: 'bg',
})

module.exports = BackendApp
