const Module = require('../lib/module')


class ModuleImporter extends Module {
    /**
    * @param {App} app - The application object.
    */
    constructor(...args) {
        super(...args)

        this.app.on('bg:importer:import', async() => {
            this.app.setState({importer: {progress: 0.1, status: this.app.$t('Importing data')}})
            await this.app.store.import('vrienden')
            this.app.setState({importer: {progress: 0.25, status: this.app.$t('vrienden collection imported')}})
            await this.app.store.import('deelnames')
            this.app.setState({importer: {progress: 0.50, status: this.app.$t('deelnames collection imported')}})
            await this.app.store.import('atleten')
            this.app.setState({importer: {progress: 0.75, status: this.app.$t('atleten collection imported')}})
            await this.app.store.import('wedstrijden')
            this.app.setState({importer: {progress: 0.9, status: this.app.$t('wedstrijden collection imported')}})
            await this.app.store.generateFilterData()
            this.app.setState({importer: {imported: true, progress: 1, status: this.app.$t('Filters generated')}}, {persist: true})

            const res = await this.app.store.db.cities.toArray()
            this.app.setState({map: {filters: {city: {options: res}}}}, {persist: true})
        })
    }


    _initialState() {
        return {
            imported: false,
            progress: 0,
            status: '',
        }
    }

    _ready() {
        this.app.setState({importer: {status: this.app.$t('inactive')}})
    }
}

module.exports = ModuleImporter
