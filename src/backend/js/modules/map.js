const Module = require('../lib/module')


class ModuleImporter extends Module {
    /**
    * @param {App} app - The application object.
    */
    constructor(...args) {
        super(...args)
    }


    _initialState() {
        return {
            filters: {
                city: {
                    options: [],
                    selected: {id: null, name: null},
                },
            },
            markers: [],
            zoom: 13,
        }
    }

    async _ready() {
        if (!this.app.state.map.filters.city.options.length) {
            const res = await this.app.store.db.cities.toArray()
            this.app.setState({map: {filters: {city: {options: res}}}}, {persist: true})
        }
    }
}

module.exports = ModuleImporter
