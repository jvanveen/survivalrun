const {_extend} = require('util')

const addsrc = require('gulp-add-src')
const browserify = require('browserify')
const buffer = require('vinyl-buffer')

const cleanCSS = require('gulp-clean-css')
const composer = require('gulp-uglify/composer')
const concat = require('gulp-concat')
const connect = require('connect')
const envify = require('gulp-envify')

const gulp = require('gulp-help')(require('gulp'), {})
const gutil = require('gulp-util')
const http = require('http')
const ifElse = require('gulp-if-else')
const livereload = require('gulp-livereload')

const minifier = composer(require('uglify-es'), console)
const mount = require('connect-mount')
const notify = require('gulp-notify')
const path = require('path')

const sass = require('gulp-sass')
const serveIndex = require('serve-index')
const serveStatic = require('serve-static')
const size = require('gulp-size')
const source = require('vinyl-source-stream')
const sourcemaps = require('gulp-sourcemaps')
const watchify = require('watchify')

// Browserify instance caching.
let BUNDLERS = {bg: null, fg: null, tab: null}


class Helpers {

    constructor(settings) {
        this.settings = settings
    }


    /**
    * A generic Browserify task that can be used for multiple entrypoints.
    * @param {Object} opts - Options to pass to Browserify compile function.
    * @param {String} opts.bundle - Name of the entrypoint.
    * @param {Function} opts.cb - Callback when the task is done.
    * @param {Array} [opts.entries] - Optional extra entries.
    * @param {Object} [opts.env] - Variables to search and replace in the source.
    */
    jsEntry({bundle, entries = [], env = {}, callback, sourcemap = false, minify = false}) {
        if (!BUNDLERS[bundle]) {
            BUNDLERS[bundle] = browserify({
                cache: {},
                debug: sourcemap,
                entries: entries,
                packageCache: {},
            })
            if (this.settings.LIVERELOAD) BUNDLERS[bundle].plugin(watchify)
        }
        BUNDLERS[bundle].ignore('process')
        BUNDLERS[bundle].bundle()
            .on('error', notify.onError('Error: <%= error.message %>'))
            .on('end', () => {callback()})
            .pipe(source(`${bundle}.js`))
            .pipe(buffer())
            .pipe(ifElse(sourcemap, () => sourcemaps.init({loadMaps: true})))
            .pipe(envify(env))
            .pipe(ifElse(minify, () => minifier()))
            .pipe(ifElse(sourcemap, () => sourcemaps.write('./')))
            .pipe(gulp.dest(path.join(this.settings.BUILD_DIR, 'js')))
            .pipe(size(_extend({title: `${bundle}.js`}, this.settings.SIZE_OPTIONS)))
    }


    /**
    * Generic SCSS task that can be used for multiple entrypoints.
    * @param {Object} opts - Options to pass to scss render function.
    * @param {String} opts.bundle - Name of the scss entrypoint.
    * @param {String} [opts.extraSource] - Pattern to add extra SCSS to the bundle.
    * @param {String} [opts.sourcemap] - Whether to generate sourcemaps.
    * @returns {Function} - SCSS function to use.
    */
    scssEntry({bundle, extraSource = false, sourcemap = false}) {
        return gulp.src(path.join(this.settings.SRC_DIR, 'shared', 'scss', `${bundle}.scss`))
            .pipe(ifElse(extraSource, () => addsrc(extraSource)))
            .pipe(ifElse(sourcemap, () => sourcemaps.init({loadMaps: true})))
            .pipe(sass({
                includePaths: this.settings.NODE_PATH,
                sourceMap: false,
                sourceMapContents: false,
            }))
            .on('error', notify.onError('Error: <%= error.message %>'))
            .pipe(concat(`${bundle}.css`))
            .pipe(ifElse(this.settings.PRODUCTION, () => cleanCSS({advanced: true, level: 2})))
            .pipe(ifElse(sourcemap, () => sourcemaps.write('./')))
            .pipe(gulp.dest(path.join(this.settings.BUILD_DIR, 'css')))
            .pipe(size(_extend({title: `scss-${bundle}`}, this.settings.SIZE_OPTIONS)))
            .on('end', () => {
                if (this.settings.LIVERELOAD) livereload.changed(`${bundle}.css`)
            })
    }


    /**
    * Fire up a development server that serves docs
    * and the build directory.
    */
    devServer() {
        gutil.log('Starting development server. Hit Ctrl-c to quit.')
        const app = connect()
        livereload.listen({silent: false})
        app.use(serveStatic(this.settings.BUILD_DIR))
        app.use('/', serveIndex(this.settings.BUILD_DIR, {icons: false}))
        app.use(mount('/docs', serveStatic(path.join(__dirname, 'build', 'docs'))))
        http.createServer(app).listen(8999)
    }
}

module.exports = Helpers
