const {_extend} = require('util')
const path = require('path')
const addsrc = require('gulp-add-src')
const argv = require('yargs').argv
const childExec = require('child_process').exec
const composer = require('gulp-uglify/composer')
const concat = require('gulp-concat')
const del = require('del')
const flatten = require('gulp-flatten')
const fuet = require('gulp-fuet')
const gulp = require('gulp-help')(require('gulp'), {})
const gutil = require('gulp-util')
const livereload = require('gulp-livereload')
const ifElse = require('gulp-if-else')
const imagemin = require('gulp-imagemin')
const insert = require('gulp-insert')
const mkdirp = require('mkdirp')
const minifier = composer(require('uglify-es'), console)
const notify = require('gulp-notify')
const rename = require('gulp-rename')
const runSequence = require('run-sequence')
const size = require('gulp-size')
const svgo = require('gulp-svgo')

const WITHDOCS = argv.docs ? argv.docs : false

const Helpers = require('./gulphelpers')

let settings = {}

settings.BUILD_DIR = process.env.BUILD_DIR || path.join('./', 'build')
settings.LIVERELOAD = false
settings.PACKAGE = require('./package')
settings.NODE_PATH = path.join(__dirname, 'node_modules') || process.env.NODE_PATH
settings.SRC_DIR = path.join('./', 'src')
settings.TEMP_DIR = path.join(settings.BUILD_DIR, '__tmp')
settings.VERBOSE = argv.verbose ? true : false

// Force production mode when running certain tasks from
// the commandline or when using a deploy command.
if (argv._[0] && (['deploy', 'build-dist'].includes(argv._[0]) || argv._[0].includes('deploy'))) {
    settings.PRODUCTION = true
    // Force NODE_ENV to production for envify.
    process.env.NODE_ENV = 'production'
} else {
    // Production mode is on when NODE_ENV environmental var is set.
    settings.PRODUCTION = argv.production ? argv.production : (process.env.NODE_ENV === 'production')
    if (!process.env.NODE_ENV) process.env.NODE_ENV = 'development'
}

settings.NODE_ENV = process.env.NODE_ENV

// Notify developer about some essential build flag values.
gutil.log('BUILD FLAGS:')
gutil.log(`- PRODUCTION: ${settings.PRODUCTION}`)
gutil.log(`- VERBOSE: ${settings.VERBOSE}`)


const helpers = new Helpers(settings)


gulp.task('assets', 'Copy assets to the build directory.', () => {
    const robotoPath = path.join(settings.NODE_PATH, 'roboto-fontface', 'fonts', 'roboto')
    return gulp.src(path.join(robotoPath, '{Roboto-Light.woff2,Roboto-Regular.woff2,Roboto-Medium.woff2}'))
        .pipe(flatten({newPath: './fonts'}))
        .pipe(addsrc('./src/frontend/img/{*.png,*.jpg,*.gif}', {base: './src/frontend'}))
        .pipe(ifElse(settings.PRODUCTION, imagemin))
        .pipe(addsrc('./src/shared/i18n/**/*.js', {base: './src/'}))
        .pipe(gulp.dest(settings.BUILD_DIR))
        .pipe(size(_extend({title: 'assets'}, settings.SIZE_OPTIONS)))
        .pipe(ifElse(settings.LIVERELOAD, livereload))
})


gulp.task('build', 'Make a branded unoptimized development build.', (done) => {
    runSequence([
        'assets',
        'js-backend',
        'js-frontend',
        'templates',
        'translations',
        'html',
        'scss-app',
        'scss-vendor',
        'js-data',
        'js-vendor',
    ], done)
})


gulp.task('build-clean', 'Clean the build directory', async() => {
    await del([path.join(settings.BUILD_DIR, settings.BRAND_TARGET, settings.BUILD_TARGET, '**')], {force: true})
    mkdirp(path.join(settings.BUILD_DIR, settings.BRAND_TARGET, settings.BUILD_TARGET))
})


gulp.task('html', 'Preprocess and build application HTML.', () => {
    return gulp.src(path.join('src', 'shared', 'html', 'index.html'))
        .pipe(flatten())
        .pipe(gulp.dest(settings.BUILD_DIR))
        .pipe(ifElse(settings.LIVERELOAD, livereload))
})


gulp.task('js-backend', 'Generate webview target javascript.', (done) => {
    helpers.jsEntry({
        bundle: 'backend',
        callback: () => {
            if (settings.LIVERELOAD) livereload.changed('backend.js')
            done()
        },
        entries: [path.join(settings.SRC_DIR, 'backend', 'js', 'index.js')],
        env: {
            NODE_ENV: settings.NODE_ENV,
            VERBOSE: false,
        },
    })
})


gulp.task('js-frontend', 'Generate webview target javascript.', (done) => {
    helpers.jsEntry({
        bundle: 'frontend',
        callback: () => {
            if (settings.LIVERELOAD) livereload.changed('frontend.js')
            done()
        },
        entries: [path.join(settings.SRC_DIR, 'frontend', 'js', 'index.js')],
        env: {
            NODE_ENV: settings.NODE_ENV,
            VERBOSE: false,
        },
    })
})


gulp.task('js-vendor', 'Generate javascript dependencies file.', (done) => {
    helpers.jsEntry({
        bundle: 'vendor',
        callback: () => {
            if (settings.LIVERELOAD) livereload.changed('vendor.js')
            done()
        },
        entries: [
            path.join(settings.SRC_DIR, 'shared', 'js', 'vendor.js')
        ],
        env: {
            NODE_ENV: settings.NODE_ENV,
        },
    })
})

gulp.task('js-data', 'Generate all survivorrun data', [
    'js-data-atleten',
    'js-data-deelnames',
    'js-data-vrienden',
    'js-data-wedstrijden',
], (done) => {

})


gulp.task('js-data-atleten', 'Generate javascript dependencies file.', (done) => {
    helpers.jsEntry({
        bundle: 'data-atleten',
        callback: () => done(),
        entries: [path.join(settings.SRC_DIR, 'shared', 'js', 'data', 'atleten.js')],
    })
})


gulp.task('js-data-deelnames', 'Generate javascript dependencies file.', (done) => {
    helpers.jsEntry({
        bundle: 'data-deelnames',
        callback: () => done(),
        entries: [path.join(settings.SRC_DIR, 'shared', 'js', 'data', 'deelnames.js')],
    })
})

gulp.task('js-data-vrienden', 'Generate javascript dependencies file.', (done) => {
    helpers.jsEntry({
        bundle: 'data-vrienden',
        callback: () => done(),
        entries: [path.join(settings.SRC_DIR, 'shared', 'js', 'data', 'vrienden.js')],
    })
})

gulp.task('js-data-wedstrijden', 'Generate javascript dependencies file.', (done) => {
    helpers.jsEntry({
        bundle: 'data-wedstrijden',
        callback: () => done(),
        entries: [path.join(settings.SRC_DIR, 'shared', 'js', 'data', 'wedstrijden.js')],
    })
})


gulp.task('scss-app', 'Generate css application file.', () => {
    return helpers.scssEntry({
        bundle: 'app',
        extraSource: path.join(settings.SRC_DIR, 'shared', 'components', '**', '*.scss'),
        sourcemap: !settings.PRODUCTION,
    })
})


gulp.task('scss-vendor', 'Generate css vendor file.', () => {
    return helpers.scssEntry({
        bundle: 'vendor',
        sourcemap: false,
    })
})


gulp.task('templates', 'Generate compiled vue templates javascript file.', () => {
    gulp.src(path.join(settings.SRC_DIR, 'shared', 'components', '**', '*.vue'))
        .pipe(fuet({
            commonjs: false,
            namespace: 'global.templates',
            pathfilter: ['src', 'components', 'shared'],
        }))
        .on('error', notify.onError('Error: <%= error.message %>'))
        .pipe(ifElse(settings.PRODUCTION, () => minifier()))
        .on('end', () => {
            if (settings.LIVERELOAD) livereload.changed('templates.js')
        })
        .pipe(concat('templates.js'))
        .pipe(insert.prepend('global.templates={};'))
        .pipe(gulp.dest(path.join(settings.BUILD_DIR, 'js')))
        .pipe(size(_extend({title: 'templates'}, settings.SIZE_OPTIONS)))
})


gulp.task('translations', 'Generate translations', (done) => {
    return gulp.src('./src/shared/js/i18n/*.js', {base: './src/js/'})
        .pipe(concat('translations.js'))
        .pipe(ifElse(settings.PRODUCTION, () => minifier()))
        .pipe(gulp.dest(path.join(settings.BUILD_DIR, 'js')))
        .pipe(size(_extend({title: 'js-translations'}, settings.SIZE_OPTIONS)))
        .pipe(ifElse(settings.LIVERELOAD, livereload))
})


gulp.task('watch', 'Start development server and watch for changes.', () => {
    settings.LIVERELOAD = true
    helpers.devServer()

    if (WITHDOCS) {
        gutil.log('Watching documentation')
        gulp.watch([
            path.join(__dirname, '.jsdoc.json'),
            path.join(__dirname, 'README.md'),
            path.join(__dirname, 'docs', 'manuals', '**'),
        ], () => {
            gulp.start('docs')
        })
    }


    gulp.watch(path.join(__dirname, 'src', 'shared', 'js', 'i18n', '**', '*.js'), ['translations'])

    gulp.watch([
        path.join(__dirname, 'src', 'shared', 'js', '**', '*.js'),
        path.join(__dirname, 'src', 'backend', 'js', '**', '*.js'),
        `!${path.join(__dirname, 'src', 'shared', 'js', 'vendor.js')}`,
        `!${path.join(__dirname, 'src', 'shared', 'js', 'i18n', '*.js')}`,
    ], ['js-backend'])


    gulp.watch([
        path.join(__dirname, 'src', 'shared', 'js', '**', '*.js'),
        path.join(__dirname, 'src', 'frontend', 'js', '**', '*.js'),
        path.join(__dirname, 'src', 'shared', 'components', '**', '*.js'),
        `!${path.join(__dirname, 'src', 'shared', 'js', 'vendor.js')}`,
        `!${path.join(__dirname, 'src', 'shared', 'js', 'i18n', '*.js')}`,
    ], ['js-frontend'])

    gulp.watch(path.join(__dirname, 'src', 'shared', 'js', 'vendor.js'), ['js-vendor'])
    gulp.watch(path.join(__dirname, 'src', 'shared', 'js', 'data', 'index.js'), ['js-data'])
    // Watch files related to working on the html and css.
    gulp.watch(path.join(__dirname, 'src', 'shared', 'html', 'index.html'), ['html'])
    gulp.watch([
        path.join(__dirname, 'src', 'shared', 'scss', '**', '*.scss'),
        `!${path.join(__dirname, 'src', 'shared', 'scss', 'vendor.scss')}`,
        path.join(__dirname, 'src', 'shared', 'components', '**', '*.scss'),
    ], ['scss-app'])

    gulp.watch(path.join(__dirname, 'src', 'shared', 'scss', 'vendor.scss'), ['scss-vendor'])
    gulp.watch(path.join(__dirname, 'src', 'shared', 'components', '**', '*.vue'), ['templates'])
    gulp.watch(path.join(__dirname, 'src', 'js', 'i18n', '**', '*.js'), ['translations'])
})
